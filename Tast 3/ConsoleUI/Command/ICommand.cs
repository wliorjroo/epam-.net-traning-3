﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleUI.Command
{
    interface ICommand
    {
        void Execute();
    }
}
