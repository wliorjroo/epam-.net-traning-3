﻿using ConsoleUI.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleUI.CommandParser
{
    interface ICommandParser
    {
        ICommand ParseCommand(string comandWithArgs);
    }
}
