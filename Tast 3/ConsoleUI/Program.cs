﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleUI
{
    static class Program
    {
        private static bool _isExit = false;
        //private static 

        static void Main(string[] args)
        {
            while (!_isExit)
            {
                try
                {
                    Console.ReadLine();
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception.Message);
                }
            }
        }
    }
}
